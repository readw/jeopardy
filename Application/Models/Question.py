class Question:

    def __init__(self, category, question, answer, points=200):
        self.points = points
        self.category = category
        self.question = question
        self.answer = answer

        self._isAnswered = False

    def IsAnswerCorrect(self, userAnswer):
        return userAnswer == self.answer

    def SetAnswered(self):
        self._isAnswered = True

    def IsAnswered(self):
        return self._isAnswered;