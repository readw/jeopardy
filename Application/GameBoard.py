from random import sample
from Application.Models.Question import Question

class GameBoard:
    def __init__(self):
        self._allQuestions = self._GetQuestions();
        self._allCategories = self._GetCategories();
        
        self.boardQuestions = {};

    def CreateGameBoard(self):
        for category in self._allCategories:
            self.boardQuestions[category] = [];

        for question in self._allQuestions:
            if (question.points not in [boardQuestion.points for boardQuestion in self.boardQuestions[question.category]]):
                self.boardQuestions[question.category].append(question);
    
    def QuestionsActive(self):
        boardQuestionValues = list(self.boardQuestions.values())
        allBoardQuestions = []
        [allBoardQuestions.extend(question) for question in boardQuestionValues];
        return len([answeredQuestion for answeredQuestion in allBoardQuestions if not answeredQuestion.IsAnswered()]);

    def _GetCategories(self):
        categories = set([question.category for question in self._allQuestions])
        return sample(categories, len(categories) if len(categories) < 6 else 6);
        
    def _GetQuestions(self):
        return [
            Question("Video Games", "What is the name of the final course of all Mario Kart video games?", "Rainbow Road", 200),
            Question("Video Games", "Mario originated as a character in which video game?", "Donkey Kong", 400),
            Question("Video Games", "Solid Snake is the hero of the famous video game franchise?", "The Metal Gear", 600),
            Question("Video Games", "Which famous video game franchise is the game 'V-Bucks' from?", "Fortnite", 800),
            Question("Video Games", "Nintendo began as a company that sold which products?", "Playing Cards", 1000),

            Question("Music", "Who won the 2010 BRIT Award for British Male Solo Artist?", "Dizzee Rascal", 200),
            Question("Music", "Which band released a 2003 album entitled Youth and Young Manhood?", "Kings of Leon", 400),
            Question("Music", "Former Spice Girl Melanie Chisholm dueted with which Canadian singer-songwriter on the 1998 single When Youre Gone?", "Bryan Adams", 600),
            Question("Music", "Which band released an 1974 album entitled Crime of the Century?", "Supertramp", 800),
            Question("Music", "Elvis Presleys manager, Andreas Cornelis van Kujik, was better known by what name?", "Colonel Tom Parker", 1000),

            Question("Film", "What are the dying words of Charles Foster Kane in Citizen Kane?", "Rosebud", 200),
            Question("Film", "Who played Mrs. Robinson in The Graduate?", "Anne Bancroft", 400),
            Question("Film", "What was the first feature-length animated movie ever released?", "Snow White and the Seven Dwarfs", 600),
            Question("Film", "In The Matrix, does Neo take the blue pill or the red pill?", "Red", 800),
            Question("Film", "For what movie did Tom Hanks score his first Academy Award nomination?", "Big", 1000)
        ];