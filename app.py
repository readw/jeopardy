from time import sleep

from Application.GameBoard import GameBoard
from Application.Models.Player import Player

class Jeopardy(object):
    def __init__(self):
        self.player1 = Player()
        self.player2 = Player()

        self.currentPlayer = 1;

        self.isGameEnd = False

        self.gameBoard = GameBoard();

    def StartGame(self):
        print("WELCOME TO THE GAME OF JEOPARDY BY WILLIAM READ!!!\n")
        self.gameBoard.CreateGameBoard()

    def ShowGameBoard(self):
        boardQuestions = _jeopardy.gameBoard.boardQuestions
        for category in boardQuestions:
            print("{0}: {1}".format(category, [boardQuestion.points for boardQuestion in boardQuestions[category] if not boardQuestion.IsAnswered()]))

    def GetCurrentQuestion(self, category, points):
        boardQuestions = self.gameBoard.boardQuestions
        camelCaseCategory = category.title()

        if not str(camelCaseCategory).lower() in [str(category).lower() for category in boardQuestions.keys()]:
            return

        if not points in [str(question.points) for question in boardQuestions[camelCaseCategory] if not question.IsAnswered()]:
            return;

        return [question for question in boardQuestions[camelCaseCategory] if str(question.points) == points][0]

    def SetCurrentQuestionAsAnswered(self, currentQuestion):
        [question for question in self.gameBoard.boardQuestions[currentQuestion.category] if question.points == currentQuestion.points][0].SetAnswered()

if (__name__ == "__main__"):
    
    _jeopardy = Jeopardy();

    _jeopardy.StartGame();

    while not _jeopardy.isGameEnd:
        print("CURRENT SCORES: PLAYER 1 - ${0}, PLAYER 2 - ${1}\n".format(_jeopardy.player1.score, _jeopardy.player2.score))

        print("PLAYER {0} Turn\n".format(_jeopardy.currentPlayer))

        print(_jeopardy.ShowGameBoard())

        selectedCategory = str(input("SELECT A CATEGORY: "))
        selectedPoints = str(input("FOR HOW MUCH?: "))

        currentQuestion = _jeopardy.GetCurrentQuestion(selectedCategory, selectedPoints);

        if currentQuestion == None:
            print("THAT QUESTION IS INVALID!\n")
            continue

        print("\nQUESTION: {0}".format(currentQuestion.question))
        answer = str(input("ANSWER: "))

        if (answer.lower() == currentQuestion.answer.lower()):
            print("CONGRATULATIONS YOU WON ${0}".format(currentQuestion.points));
            if (_jeopardy.currentPlayer == 1):
                _jeopardy.player1.AddPoints(currentQuestion.points);
            else:
                _jeopardy.player2.AddPoints(currentQuestion.points);
        else:
            print("INCORRECT! THE CORRECT ANSWER WAS: {0}".format(currentQuestion.answer));

        _jeopardy.SetCurrentQuestionAsAnswered(currentQuestion);
        
        if (_jeopardy.currentPlayer == 1):
            _jeopardy.currentPlayer = 2
        else:
            _jeopardy.currentPlayer = 1

        if _jeopardy.gameBoard.QuestionsActive() <= 0:
            if (_jeopardy.player1.score > _jeopardy.player2.score):
                print("\nPLAYER 1 WINS ${0}!!!".format(_jeopardy.player1.score))
            if (_jeopardy.player1.score < _jeopardy.player2.score):
                print("\nPLAYER 2 WINS ${0}!!!".format(_jeopardy.player1.score))
            else:
                print("\nIT'S A DRAW!")

            _jeopardy.isGameEnd = True
            
