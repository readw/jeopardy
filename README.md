# Jeopardy

## Introduction

This is a Python Jeopardy Console Application which you can play with two people.

When you start you will be displayed a gameboard of appropriate questions, enter the name of the Category followed by the number of Points you want to try and answer the question for, score more points than your opponent to Win the Game.

If you get the question right you get the points otherwise it moves to the next player removing the available option.

## Playing the Game

In order to play the game you will need the following:

- Installation of Python@3.10.5 or greater.

To start the game you will need to do one of the following:

### Using Terminal

- Download the application.
- Open up terminal at your Python installation directory.
- Run the following command: ```python "{APPLICATION_FILEPATH}/app.py"```

### Using Python IDLE/VS Code

- Download the application.
- Open up app.py using the IDLE.
- Navigate to Run -> Run Module (or press F5)

### Using VS Code

- Download the application.
- Open up the downloaded file with VS Code.
- Install or enable the Python extension and select the appropriate version.
- Navigate to ```app.py``` and click the Play Button to start the application.

## Adding Questions

You may add additional questions from the ```_GetQuestions()``` method in Gameboard.py by adding additional Questions to the returned array using the structure below:

```new Question({CATEGORY}, {QUESTION}, {ANSWER}, {POINTS})```

| Argument | Type | Example |
|-|-|-|
|CATEGORY|String|```"Video Games"```|
|QUESTION|String|```"What is the name of the final course of all Mario Kart video games?"```|
|ANSWER|String|```"Rainbow Road"```|
|POINTS|Int|```200```|

NOTE: If you have more than 6 questions or duplicate point totals, the application will pick a random sample of 6 chategories and add questions with point totals it hasn't selected yet and build an appropirate grid, making each game random and unique.

## Developer Notes

This is an open source project and free to use from anyone wanting to learn Python. This is a very basic form of what this application can be so feel free to take and expand upon the above and implement your own ideas a few examples could be:

- Implement access to a Questions/Answers API (example: <http://jservice.io/api/random>) and retrieve a list of questions that can be used rather than statically adding them each time.
- Implement a double point score/bet points mechanic to mirror the events of the real game.
- Create a UI using TKinter or other library to display the game board and handle user inputs in a much cleaner display.
- You can even try implementing a similar solution within another language and compare the differences to try and achieve the same solution to help grasp your understanding of that language.
